<?php

namespace TestBundle\DependencyInjection;

/**
 * Description of XMLOrdersLoader
 *
 * @author Hadrien
 */
class XMLOrdersLoader {
   
    private $flux;
    
    public function __construct() {
        
        $this->flux  = simplexml_load_file('http://test.lengow.io/orders-test.xml');
        
    }
    
    public function getFlux() {
        return $this->flux;
    }
}

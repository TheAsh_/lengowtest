<?php

namespace TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LengowOrder
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="TestBundle\Entity\LengowOrderRepository")
 */
class LengowOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", length=255)
     */
    private $order_id;

    /**
     * @var string
     *
     * @ORM\Column(name="marketplace", type="string", length=255)
     */
    private $marketplace;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_amount", type="integer")
     */
    private $order_amount;

    /**
     * @var string
     *
     * @ORM\Column(name="order_items", type="integer")
     */
    private $order_items;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order_id
     *
     * @param integer $orderId
     * @return LengowOrder
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;
    
        return $this;
    }

    /**
     * Get order_id
     *
     * @return string
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * Set marketplace
     *
     * @param string $marketplace
     * @return LengowOrder
     */
    public function setMarketplace($marketplace)
    {
        $this->marketplace = $marketplace;
    
        return $this;
    }

    /**
     * Get marketplace
     *
     * @return string 
     */
    public function getMarketplace()
    {
        return $this->marketplace;
    }

    /**
     * Set order_amount
     *
     * @param integer $orderAmount
     * @return LengowOrder
     */
    public function setOrderAmount($orderAmount)
    {
        $this->order_amount = $orderAmount;
    
        return $this;
    }

    /**
     * Get order_amount
     *
     * @return integer 
     */
    public function getOrderAmount()
    {
        return $this->order_amount;
    }

    /**
     * Set order_items
     *
     * @param string $order_items
     * @return LengowOrder
     */
    public function setOrderItems($order_items)
    {
        $this->order_items = $order_items;
    
        return $this;
    }

    /**
     * Get order_items
     *
     * @return integer
     */
    public function getOrderItems()
    {
        return $this->order_items;
    }
    
    public function hydrate($tab) {
        foreach ($tab as $key => $value) {
            $pos = strrpos($key, '_');
            if( $pos !== false ){
                $key = substr($key, 0, $pos) . ucfirst(substr($key, $pos+1));
            }
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }
    
    public function toJson() {

        return json_encode(
                array(
                    'marketplace' => $this->marketplace,
                    'orderid' => $this->order_id,
                    'orderamount' => $this->order_amount,
                    'orderitems' => $this->order_items)
        );
    }
}

<?php

namespace TestBundle\Controller;

use TestBundle\Entity\LengowOrder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use APY\DataGridBundle\Grid\Source\Entity;
use TestBundle\DependencyInjection\XMLOrdersLoader;
use TestBundle\Form\LengowOrderType;

/**
 * Description of LengowOrderController
 *
 * 
 */
class LengowOrderController extends Controller {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('TestBundle:LengowOrder');
        $orders = $repo->getOrdersOrderedById();
        if (sizeof($orders) === 0) {
            $xml = $this->container->get('lengow_test');
            $flux = $xml->getFlux();
            $orders = $flux->orders;
            foreach ($orders as $value) {
                foreach ($value as $val) {
                    $ord = new LengowOrder();
                    $ord->hydrate($val);
                    $em->persist($ord);
                }
            }
            $em->flush();
        }
        $source = new Entity('TestBundle:LengowOrder');
        $grid = $this->get('grid');
        $grid->setSource($source);
        return $grid->getGridResponse('TestBundle:Default:index.html.twig');
    }

    public function addAction(Request $req) {
        $order = new LengowOrder();

        $form = $this->createForm(new LengowOrderType(), $order, array(
            'action' => $this->generateUrl('order_add')
        ));
        $form->handleRequest($req);
        if ($form->isValid()) {
            try {
                $em = $this->getDoctrine()->getManager();
                $repo = $em->getRepository('TestBundle:LengowOrder');
                if ($repo->getOrderById($order->getOrderId()) === null) {
                    $em->persist($order);
                    $em->flush();
                    $req->getSession()->getFlashBag()->add('success', 'Order added');
                    return $this->redirect($this->generateUrl('homepage'));
                } else {
                    $req->getSession()->getFlashBag()->add('danger', 'Order still exists');
                }
            } catch (\Doctrine\DBAL\DBALException $e) {
                $req->getSession()->getFlashBag()->add('danger', 'add failed :'
                        . PHP_EOL . $e->getMessage());
            }
        }
        return $this->render('TestBundle:Default:add.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function getAllOrdersAction() {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('TestBundle:LengowOrder');
        $orders = $repo->getOrdersOrderedById();
        $data = [];
        foreach($orders as $order){
            $data[] = $order->toJson();
        }

        return $this->render('TestBundle:Default:getAllOrders.html.twig', array(
            'orders' => $data
        ));
    }
    
    public function getOrderAction($id) {
        $em = $this->getDoctrine()->getManager();
        $repo = $em->getRepository('TestBundle:LengowOrder');
        $order = $repo->getOrderById($id);
        
        return $this->render('TestBundle:Default:getOrder.html.twig', array(
            'order' => $order->toJson()
        ));
    }

}
